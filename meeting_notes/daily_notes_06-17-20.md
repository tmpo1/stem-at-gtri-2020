# Daily Notes 06/17/20

## Updates

### Logistics

 - no updates
 
### App Dev

 - Self-Distribution: Sanjay
   - Looked at APK extractor
     - disassembled and methodologies inspected
 - Basic Comms: Axel
   - Doc appt
   - update tmrw

### ModSim

 - Base Grid: Daniel & Jacqui
   - Basic NxN grid, with adj mat


## Tasking

### Will

 - meeting notes on-line

### App Dev

 - working on APK dist
 - working on basic messaging

### ModSim

 - develop static grid of nodes
   - working on dynamic size
   - looking into representing edge weights in addtl matrices
   - looking into python libs
