# Kickoff Notes

## Project Ideas

 - Power Grid Analysis and Stability
   - Physical components, easy to understand
 - Internet traffic modeling
 - FireChat: Interesting App
   - Emergency Mesh Network
     - Phones as intermediate transmission nodes
	 - Bluetooth/WiFi as communication links
   - Can't self replicate
     - phones without the app, can't get it after coms are lost
 - Important Parameters
   - node density
   - connection density
   - node transience
   
## Selected Idea

Develop a FireChat like app that can self-replicate and do modsim on such
a network to investigate relevant properties.
   
## Organizational Structure
   
### App Development

Axel & Sanjay

### ModSim

Daniel & Jacqui

## Tasks for Next Meeting

### App Development

 - Bluetooth support in emulators
   - multi device support
 - App self-distribution
 
### ModSim

 - Graph theory familiarization
   - BFS
   - DFS
   - Dijkstras
 - Begin investigation of relevant edge params
   - latency?
   - bandwidth?