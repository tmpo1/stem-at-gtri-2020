# Daily Notes 06/17/20

## Updates

### Logistics

 - Mid-Way Demo - July 2nd
   - 1400hrs
   - Mid-Way Demo huge success
   
 - 
   
 - Ticket open with IT
   - Opening per person tickets
 
### App Dev

 - Self-Distribution: Sanjay
   - before replication, there's popups
   - test app, sends intent
   - transitioned to material design look
     - instruction sliders
	 - using Maven, made by Google
	 - getting material design/sliders
   - UI mostly done
   - APK now giving an app not installed error
     - error on repeated installs
	 
 - Basic Comms: Axel
   - bluetooth app working
     - just text for now
   - file sharing in beta
   - text transmission pushed
   - UI fixes
   - file choosing errors
     - now sending files consistently
	 - little buginess left
	 
 - integration
   - put Sanjay's stuff into Axel's stuff
   - jk put Axel's stuff into Sanjay's stuff

### ModSim

 - Base Grid: Daniel & Jacqui
   - shortest path finding 
     - works to show shortest path length
	 - now shows path as well
	 - looking into dynamic connectivity
	   - add/delete edges
	   - algorithm called Quickfind
	   - added to RGs
	   - combining Dij/QF
   - density testing
     - success of finding node based on density
	   - incl different number of nodes
     - 1k trials on 10 - 200 nodes
	 - included aspect ratios and graphs included
	   - regression?
	   - 10-14th order regression -> included non-poly component
	   - theorize what the actual function is?
	 - compare runtimes/lengths
	 - applied regressions -> got equations
	 - AR pl Succ	
     - AR testing done
     - looking at Dynnetx 	 
   - PyCx
     - using library code to model network graphs
	 - adds nodes/edges over time
	 - Jacqui working on node/edge drop out
	 - also running Dijkstras on dynamic networks
	 - similar data points
	   - success rate

## Tasking

### Will

 - Mid-Way Presentation
   - July 2nd

### App Dev

 - Self-Dist
   - integrate apps
   - error popups
 - Comms
   - O.K.
   - working on better UI
   - BT exceptions
 - Long Term
   - phone hopping

### ModSim

 - Dijkstras
   - working to show the actual path, not just length
 - Density testing data collection done
   - will run analysis
 - Looked into aspect ratio testing
   - DFS search time
   - will also do BFS
