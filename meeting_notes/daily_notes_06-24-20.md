# Daily Notes 06/17/20

## Updates

### Logistics

 - Mid-Way Demo - July 2nd
 
### App Dev

 - Self-Distribution: Sanjay
   - before replication, there's popups
   - test app, sends intent
 - Basic Comms: Axel
   - bluetooth app working
     - just text for now

### ModSim

 - Base Grid: Daniel & Jacqui
   - shortest path finding 
     - works to show shortest path length
	 - now shows path as well
	 - looking into dynamic connectivity
	   - add/delete edges
   - density testing
     - success of finding node based on density
	   - incl different number of nodes
     - 1k trials on 10 - 200 nodes
	 - included aspect ratios and graphs included
	   - regression?
	   - 10-14th order regression -> included non-poly component
	   - theorize what the actual function is?
	 - compare runtimes/lengths

## Tasking

### Will

 - Mid-Way Presentation
   - July 2nd

### App Dev

 - Self-Dist
   - integrate apps
   - error popups
 - Comms
   - O.K.
   - working on better UI
   - BT exceptions
 - Long Term
   - phone hopping

### ModSim

 - Dijkstras
   - working to show the actual path, not just length
 - Density testing data collection done
   - will run analysis
 - Looked into aspect ratio testing
   - DFS search time
   - will also do BFS
