# Daily Notes 06/17/20

## Updates

### Logistics

 - none
 
### App Dev

 - Self-Distribution: Sanjay
   - APK generated
   - security issue fixed
 - Basic Comms: Axel
   - connect to bluetooth successfully
   - port SMS to BT

### ModSim

 - Base Grid: Daniel & Jacqui
   - NxN adj mat + grids are working
     - radius works
	 - random graphs from network x
	 - found dijkstras impl
   - libraries on python
     - BFS/DFS search working
	   - static grids
	   - Erdos random graphs
	   - going to target specific end node

## Tasking

### Will

 - meeting notes on-line
   - done

### App Dev

 - Self-Dist
   - minor UI fixes -> bluetooth
 - Comms
   - Bluetooth

### ModSim

 - Dijkstras on random graphs
   - pluck a 1k o 10k variants out of a hat to start
   - look at connection likihoods
