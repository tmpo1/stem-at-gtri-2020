# Daily Notes 06/17/20

## Updates

### Logistics

 - reminder for morning check-ins
 
### App Dev

 - Self-Distribution: Sanjay
   - Almost working :\)
   - APK security error message
   - next step, bluetooth
 - Basic Comms: Axel
   - SMS send okay
   - next step with bluetooth

### ModSim

 - Base Grid: Daniel & Jacqui
   - NxN adj mat + grids are working
   - libraries on python
     - network x
	 - working on BFS

## Tasking

### Will

 - meeting notes on-line
   - done

### App Dev

 - Self-Dist
   - security error -> bluetooth
 - Comms
   - Bluetooth

### ModSim

 - Base Grid
   - add radius for edges
   - BFS on the network
