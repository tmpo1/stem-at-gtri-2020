# Daily Notes 06/17/20

## Updates

### Logistics

 - none
 
### App Dev

 - Self-Distribution: Sanjay
   - generated APK
   - no errors :)
 - Basic Comms: Axel
   - almost done sending via bluetooth
     - can pair etc.
	 - sends text
   - needs more MT stuff

### ModSim

 - Base Grid: Daniel & Jacqui
   - NxN adj mat + grids are working
     - radius works
	 - random graphs from network x
	 - found dijkstras impl
   - density testing
     - success of finding node based on density
	   - incl different number of nodes
     - 1k trials on 10 - 200 nodes

## Tasking

### Will

 - Mid-Way Presentation
   - July 2nd

### App Dev

 - Self-Dist
   - initiate share mechanism
 - Comms
   - Bluetooth, in progress
     - work on non-text transmission

### ModSim

 - Dijkstras on random graphs
   - pluck a 1k o 10k variants out of a hat to start
   - look at connection likihoods
